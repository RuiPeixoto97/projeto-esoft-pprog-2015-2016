package ModeloDominio;

import java.util.List;

public class Candidatura {

    private float areaPretendida;
    private int qtdConvites;
    private String empresa;
    private int telemovel;
    private String morada;

    private List<Recurso> recursos;

    public Candidatura(float areaPretendida, int qtdConvites, String empresa, int telemovel, String morada) {
        this.areaPretendida = areaPretendida;
        this.qtdConvites = qtdConvites;
        this.empresa = empresa;
        this.telemovel = telemovel;
        this.morada = morada;
    }

    public List<Recurso> getRecursos() {
        return recursos;
    }

    public float getAreaPretendida() {
        return areaPretendida;
    }

    public int getQtdConvites() {
        return qtdConvites;
    }

    public String getEmpresa() {
        return empresa;
    }

    public int getTelemovel() {
        return telemovel;
    }

    public String getMorada() {
        return morada;
    }

    public void setAreaPretendida(float areaPretendida) {
        this.areaPretendida = areaPretendida;
    }

    public void setRecursos(List<Recurso> recursos) {
        this.recursos = recursos;
    }

    public void setEmpresa(String empresa) {
        this.empresa = empresa;
    }

    public void setQtdConvites(int qtdConvites) {
        this.qtdConvites = qtdConvites;

    }

    public void setTelemovel(int telemovel) {
        this.telemovel = telemovel;
    }

    public void setMorada(String morada) {
        this.morada = morada;
    }

    @Override
    public String toString() {
        return "Candidatura de:" + empresa
                + "\nMorada:" + morada
                + "\nArea Pretendida:" + areaPretendida
                + "\nTelemovel:" + telemovel
                + "\nQuantidade de convites:" + qtdConvites;
    }

    public void registaAvaliacao(Avaliacao a) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public void novaCandidatura(Candidatura c) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public void getListaProdutos() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public void setDemonstracao(int demo) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public void validaCandidatura(int c) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

}
