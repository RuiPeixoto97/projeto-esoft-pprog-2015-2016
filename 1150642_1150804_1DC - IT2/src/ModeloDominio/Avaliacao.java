package ModeloDominio;

public class Avaliacao {

    private String decisao;
    private String txtJustificativo;

    public Avaliacao(String decisao, String txtJustificativo) {
        this.decisao = decisao;
        this.txtJustificativo = txtJustificativo;
    }

    public void valida() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public void setDecisao(String decisao) {
        this.decisao = decisao;
    }

    public void setTxtJustificativo(String txt) {
        this.txtJustificativo = txt;
    }

    public String getDecisao() {
        return decisao;
    }

    public String getTxtJustificativo() {
        return txtJustificativo;
    }

    @Override
    public String toString() {
        return "Avaliacao{" + "decisao=" + decisao + ", txtJustificativo=" + txtJustificativo + '}';
    }

    
}
