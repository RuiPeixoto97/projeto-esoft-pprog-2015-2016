package UI;

import Controller.ConfirmarRegistoUtilizadorController;

public class ConfirmarRegistoUtilizadorUI {

    public final ConfirmarRegistoUtilizadorController controller;

    public ConfirmarRegistoUtilizadorUI(ConfirmarRegistoUtilizadorController controller) {
        this.controller = controller;
    }
    
    public void run(){
        listaPedidosNaoAutenticados();
        selecionaPedido();
        decisao();
        confirma();
    }
    
    public void listaPedidosNaoAutenticados(){
        System.out.println("Utilizadores não autenticados");
        controller.listarPedidosUtilizador();
    }
    
    public void selecionaPedido(){
        System.out.println("Informacao do perfil:");
        controller.getInfoPerfil(/*email*/);
    }
    
    public void decisao(){
        controller.setDecisao(/*d*/);
    }
    
    public void confirma(){        
        controller.addUtilizador(/*u*/);
    }

}
