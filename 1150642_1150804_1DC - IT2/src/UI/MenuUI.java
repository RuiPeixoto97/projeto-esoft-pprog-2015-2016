/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package UI;

import ModeloDominio.CentroExposicoes;
import java.io.IOException;
import esoftgestaoUtils.Utils.Utils;


/**
 *
 * @author rosamarianascimentodasilva
 */

public class MenuUI
{
    private final CentroExposicoes m_ce;
    private String opcao;

    public MenuUI(CentroExposicoes ce)
    {
        m_ce = ce;
    }

    public void run() throws IOException
    {
        do
        {
            //opcao = "1";
            System.out.println("\n\n");
            System.out.println("1. Criar Exposição");
            System.out.println("2. Definir FAE");
            System.out.println("3. Atribuir Candidatura");
            System.out.println("4. Decidir Candidaturas");
            System.out.println("5. Registar Candidatura");
            System.out.println("6. Registar Utilizador");
            System.out.println("7. Confirmar Registo de Utilizador");
            
            System.out.println("0. Sair");

            opcao = Utils.readLineFromConsole("Introduza opção: ");
            
            if( opcao.equals("1") )
            {
                //CriarExposicaoUI ui = new CriarExposicaoUI(m_ce);
               // ui.run();
            }
             if( opcao.equals("2") )
            {
           //   DefinirFAEUI uiFAE = new DefinirFAEUI(m_ce);
            //  uiFAE.run();
            }

        }
        while (!opcao.equals("0") );
    }
  
}
