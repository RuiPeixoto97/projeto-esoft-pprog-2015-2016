package UI;

import Controller.CriarExposicaoController;

public class CriarExposicaoUI {

    private final CriarExposicaoController controller;

    public CriarExposicaoUI(CriarExposicaoController controller) {
        this.controller = controller;
    }

    public void run() {
        novaExposicao();
        introduzDados();
        selecionaOrganizador();
        validaExposicao();
        confirmaExposicao();
    }

    public void novaExposicao() {
        controller.getRegistoExposicoes();
    }

    public void introduzDados() {
        System.out.println("Introduza dados da exposicao");
        controller.setDados(/*titulo, desc, dataInicio,dataFim,local*/);
        controller.getListaUtilizadores();
    }

    public void selecionaOrganizador() {
        controller.addOrganizador();
    }

    public void validaExposicao() {
        controller.validaExposicao(/*e*/);
    }

    public void confirmaExposicao() {
        controller.registaExposicao(/*e*/);
    }
}
