package UI;

import Controller.AvaliarCandidaturaController;
import java.util.Scanner;

public class AvaliarCandidaturaUI {
    
    Scanner ler;
    String decisao,txt;
    private final AvaliarCandidaturaController controller;
    
    public AvaliarCandidaturaUI(AvaliarCandidaturaController controller){
        this.controller = controller;
    }
    
    public void run(){
        mostraListaExposicoes();
        selecionaExposicao();
        selecionaCandidatura();
        avaliacao();
        confirma();
    }
    
    public void mostraListaExposicoes(){
        System.out.println("Lista de exposicoes do FAE");
        controller.getListaExposicoes();
    }
    
    public void selecionaExposicao(){
        controller.getExposicao(/*e*/);
        controller.getListaCandidaturasFae(/*username*/);
    }
    
    public void selecionaCandidatura(){
        System.out.println("Candidatura:");
        controller.getCandidatura(/*c*/);
        controller.novaAvaliacao();
    }
    
    public void avaliacao(){
        System.out.println("Introduza a decisao e um texto justificativo");
        decisao = ler.nextLine();
        txt = ler.nextLine();
        controller.setAvalicao(decisao, txt);
    }
    
    public void confirma(){
        controller.addAvaliacao();
    }
}