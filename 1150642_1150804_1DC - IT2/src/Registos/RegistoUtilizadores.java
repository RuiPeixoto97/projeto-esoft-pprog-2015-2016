package Registos;

import ModeloDominio.Utilizador;
import java.util.List;

public class RegistoUtilizadores {

    private List<Utilizador> m_lUtilizador;

    public List<Utilizador> getListaUtilizadores() {
        return m_lUtilizador;
    }

    public void addUtilizador(Utilizador u) {
        m_lUtilizador.add(u);
    }

    public List<Utilizador> getM_lUtilizador() {
        return m_lUtilizador;
    }

    public void setM_lUtilizador(List<Utilizador> m_lUtilizador) {
        this.m_lUtilizador = m_lUtilizador;
    }
    

}
