package ModeloDominio;

import java.util.ArrayList;
import java.util.List;

public class Demonstracao {

    private String codigo;
    private String descricao;
    private List<Recurso> listaRecursos;

    public Demonstracao(String codigo, String descricao, List listaRecursos) {
        this.codigo = codigo;
        this.descricao = descricao;
        this.listaRecursos = new ArrayList<>(listaRecursos);
    }

    public String getCodigo() {
        return codigo;
    }

    public String getDescricao() {
        return descricao;
    }

    public List<Recurso> getListaRecursos() {
        return listaRecursos;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public void setListaRecursos(List<Recurso> listaRecursos) {
        this.listaRecursos = listaRecursos;
    }

    public void setRecurso(Recurso recurso){
        listaRecursos.add(recurso);
    }
    
    public void valida() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
}
