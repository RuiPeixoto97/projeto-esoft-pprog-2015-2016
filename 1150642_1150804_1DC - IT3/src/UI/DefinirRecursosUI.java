package UI;

import Controller.DefinirRecursosController;
import ModeloDominio.Recurso;
import java.util.Scanner;

public class DefinirRecursosUI {
    Scanner ler;
    String recurso;
    private final DefinirRecursosController controller;
    
    public DefinirRecursosUI(DefinirRecursosController controller){
        this.controller = controller;
    }
    
    public void run(){
        
    }
    
    public void solicitaRecurso(){
        controller.novoRecurso();
        System.out.println("Introduza recurso");
        recurso = ler.nextLine();
    }
    
//    public void insereRecurso(Recurso r){
//        controller.setRecurso(r);        
//    }
//    
//    public void confirma(){
//        controller.addRecurso(/*r*/);
//    }
}