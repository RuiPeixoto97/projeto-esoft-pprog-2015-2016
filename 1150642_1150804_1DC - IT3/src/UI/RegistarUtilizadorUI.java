package UI;

import Controller.RegistarUtilizadorController;
import java.util.Scanner;

public class RegistarUtilizadorUI {
    String nome,email,username,password;
    Scanner ler;
    private final RegistarUtilizadorController controller;
    
    public RegistarUtilizadorUI(RegistarUtilizadorController controller){
        this.controller = controller;
    }
    
    public void run(){
        iniciaRegisto();
        solicitaDados();
        confirma();
    }
    
    public void iniciaRegisto(){
        controller.novoUtilizador();
    }
    
    public void solicitaDados(){
        System.out.println("Introduza nome:");
        nome = ler.nextLine();
        System.out.println("Introduza email:");
        email = ler.nextLine();
        System.out.println("Introduza username:");
        username= ler.nextLine();
        System.out.println("Introduza password:");
        password = ler.nextLine();
        
        controller.setUtilizador(nome,email,username,password);
        
    }
    
    public void confirma(){
        System.out.println("Confirma os dados?");
        controller.registaUtilizador(/*u*/);
    }
}