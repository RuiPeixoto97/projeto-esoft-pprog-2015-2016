package UI;

import Controller.RegistaCandidaturaController;
import java.util.Scanner;

public class RegistaCandidaturaUI {

    Scanner ler;
    String decisao;
    private final RegistaCandidaturaController controller;

    public RegistaCandidaturaUI(RegistaCandidaturaController controller) {
        this.controller = controller;
    }

    public void run() {
        solicitaExposicao();
        escolherExposicao();
        insereDadosCandidatura();
        insereProduto();
        associaDemonstracao();
        confirmaCandidatura();
    }

    public void solicitaExposicao() {
        System.out.println("Lista de exposicoes:");
        controller.getRegistoExposicoes();
    }

    public void escolherExposicao() {
//        controller.getExposicao(/*e*/);
//        controller.novaCandidatura(/*e*/);
    }

    public void insereDadosCandidatura() {
//        controller.setDados(/*empresa, morada, telemovel, areaExpositor, numeroConvites*/);
    }

    public void insereProduto() {
        controller.getListaProdutos();
//        controller.setDadosProduto(/*nome*/);
//        controller.addProduto(/*p*/);
    }

    public void associaDemonstracao() {
        System.out.println("Deseja associar uma demonstracao?");
        decisao = ler.nextLine();
        if (decisao.equalsIgnoreCase("sim")){
            mostraDemonstracao();
            selecionaDemonstracao();
        }
    }

    public void mostraDemonstracao() {
        System.out.println("Demonstracoes:");
        controller.getListaDemonstracoes(/*Exposicao e*/);        
    }
    
    public void selecionaDemonstracao(){
//        controller.getDemonstracao();
//        controller.setDemonstracao();
    }
    
    public void confirmaCandidatura(){
        controller.registaCandidatura(/*Candidatura c*/);
    }
}
