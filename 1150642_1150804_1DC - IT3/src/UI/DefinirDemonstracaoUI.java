package UI;

import Controller.DefinirDemonstracaoController;
import java.util.Scanner;

public class DefinirDemonstracaoUI {

    Scanner ler;
    String codigo, textoDescritivo;

    private final DefinirDemonstracaoController controller;

    public DefinirDemonstracaoUI(DefinirDemonstracaoController controller) {
        this.controller = controller;
    }

    public void run() {
        mostraExposicoesOrganizador();
        selecionaExposicao();
        insereDados();
        mostraListaRecursos();
        selecionaRecurso();
        confirma();
    }

    public void mostraExposicoesOrganizador() {
        System.out.println("Selecione uma exposicao");
        controller.getListaExposicoes(/*username*/);
    }

    public void selecionaExposicao() {
        controller.novaDemonstracao();
    }

    public void insereDados() {
        System.out.println("Insira codigo");
        codigo = ler.nextLine();
        System.out.println("Insira texto descritivo");
        textoDescritivo = ler.nextLine();
        controller.setDados(codigo, textoDescritivo);
    }

    public void mostraListaRecursos() {
        System.out.println("Seleciona os recursos");
        controller.getListaRecursos();
    }

    public void selecionaRecurso() {
        controller.setRecurso(/*r*/);

    }

    public void confirma() {
        controller.addDemonstracao(/*d*/);
    }
}
